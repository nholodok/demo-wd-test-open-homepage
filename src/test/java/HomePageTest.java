import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {
    @Test
    public void testOnlinerOpen(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.onliner.by/");
        String logoXPath = "//*[@id=\"container\"]/div/div/header/div[3]/div/div[1]/a/img";
        By logoBy = By.xpath(logoXPath);
        WebElement logoElement = driver.findElement(logoBy);
        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testAmazonOpen(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com/");
        String logoXPath = "//*[@id=\"nav-logo-sprites\"]";
        By logoBy = By.xpath(logoXPath);
        WebElement logoElement = driver.findElement(logoBy);
        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testTicketproOpen(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.ticketpro.by/");
        String logoXPath = "/html/body/div[2]/header/div/div[1]/div[1]/div/a/img";
        By logoBy = By.xpath(logoXPath);
        WebElement logoElement = driver.findElement(logoBy);
        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testAlantourOpen(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://alatantour.by/");
        String logoXPath = "/html/body/header/div/div/div/div/a/img";
        By logoBy = By.xpath(logoXPath);
        WebElement logoElement = driver.findElement(logoBy);
        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }
    @Test
    public void testOlxOpen(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.olx.pl/");
        String logoXPath = "//*[@id=\"headerLogo\"]";
        By logoBy = By.xpath(logoXPath);
        WebElement logoElement = driver.findElement(logoBy);
        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }
    @Test
    public void testTripadvisorOpen(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.tripadvisor.com/");
        String logoXPath = "//*[@id=\"lithium-root\"]/header/div/nav/h1/picture/img";
        By logoBy = By.xpath(logoXPath);
        WebElement logoElement = driver.findElement(logoBy);
        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }
}